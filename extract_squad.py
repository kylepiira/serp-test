from tqdm import tqdm
import random
import json

with open('SQuAD.json') as dataset:
	data = json.load(dataset)

questions = []
for topic in tqdm(data['data']):
	for paragraph in topic['paragraphs']:
		for qas in paragraph['qas']:
			if len(qas['answers']) > 0:
				questions.append({'question': qas['question'], 'answer': qas['answers'][0]['text']})

random.shuffle(questions)

with open('qas.json', 'w+') as output:
	output.write('\n'.join([json.dumps(q) for q in questions]))