# Selenium
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import selenium
# TQDM
from tqdm import tqdm
# Caching
import functools
# General
import requests
import random
import json
import time

# Settings
headless = False

options = Options()
options.headless = headless

with open('qas.json') as dataset:
	qas = [json.loads(line) for line in dataset]
random.shuffle(qas)

driver = webdriver.Firefox(options=options)
driver.set_page_load_timeout(10)
driver.implicitly_wait(10)

def google(query):
	driver.get(f'https://www.google.com/search?q={query}')
	results = driver.find_elements_by_class_name('r')
	links = []
	for result in results:
		links.append(result.find_element_by_tag_name('a').get_attribute("href"))
	return links[:10]

def bing(query):
	driver.get(f'https://www.bing.com/search?q={query}')
	results = driver.find_elements_by_class_name('b_algo')
	links = []
	for result in results:
		try:
			links.append(result.find_element_by_tag_name('h2') \
				.find_element_by_tag_name('a') \
				.get_attribute("href"))
		except selenium.common.exceptions.NoSuchElementException:
			pass
	return links[:10]

def duckduckgo(query):
	driver.get(f'https://duckduckgo.com/?q={query}')
	results = driver.find_elements_by_class_name('result__a')
	links = []
	for result in results:
		url = result.get_attribute('href')
		if 'duckduckgo.com' not in url:
			links.append(url)
	return links[:10]

def startpage(query):
	driver.get(f'https://www.startpage.com/sp/search?q={query}')
	results = driver.find_elements_by_class_name('w-gl__result-title')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def gigablast(query):
	driver.get(f'https://gigablast.com/search?q={query}')
	results = driver.find_elements_by_class_name('title')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def yandex(query):
	driver.get(f'https://yandex.com/search/?text={query}')
	results = driver.find_elements_by_class_name('organic__url')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def lycos(query):
	driver.get(f'https://search.lycos.com/web/?q={query}')
	results = driver.find_elements_by_class_name('result-link')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def qwant(query):
	driver.get(f'https://www.qwant.com/?q={query}')
	results = driver.find_elements_by_class_name('result--web--link')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def ecosia(query):
	driver.get(f'https://www.ecosia.org/search?q={query}')
	results = driver.find_elements_by_class_name('result-title')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def cliqz(query):
	driver.get(f'https://beta.cliqz.com/search?q={query}')
	results = driver.find_elements_by_class_name('MuiLink-root')
	links = []
	for result in results:
		if result.get_attribute('href') not in links:
			links.append(result.get_attribute('href'))
	return links[:10]

def yahoo(query):
	driver.get(f'https://search.yahoo.com/search?q={query}')
	results = driver.find_elements_by_class_name('ac-algo')
	links = []
	for result in results:
		links.append(result.get_attribute('href'))
	return links[:10]

def yacy(query):
	try:
		response = requests.get(f'https://yacy.searchlab.eu/yacysearch.json?query={query}').json()
		links = [i['link'] for i in response['channels'][0]['items']][:10]
		if len(links) > 0:
			return links
		return ['about:blank']
	except json.decoder.JSONDecodeError:
		return ['about:blank']

@functools.lru_cache(maxsize=2048)
def get(link):
	return requests.get(link, timeout=10)

def check(link, target):
	try:
		response = get(link)
	except Exception as e:
		print(e)
		return False
	return target in response.text.lower()

def score(results, target):
	matches = 0
	for link in results:
		matches += check(link, target)
	return matches

engines = [
	google, bing, duckduckgo, startpage, 
	gigablast, yandex, lycos, qwant, ecosia,
	cliqz, yahoo, yacy,
]

active = []
backoffs = {engine: {'time': time.time(), 'delay': 0} for engine in engines}

def test(engine):
	results = engine('google.com')
	return len(results) > 0 and any(['google' in result for result in results])

def health():
	global active
	active = []
	print('Running health checks')
	for engine in engines:
		if backoffs[engine]['time'] < time.time():
			if test(engine):
				backoffs[engine]['delay'] = 0
				print(f'Activating: {engine.__name__}...')
				active.append(engine)
				continue
			else:
				delay = 2 * (backoffs[engine]['delay'] + 1)
				next_time = time.time() + delay
				backoffs[engine] = {
					'time': next_time,
					'delay': delay,
				}
				print(f'Disabled: {engine.__name__} failed test. Scheduled for {next_time}')
		else:
			print(f'Disabled: {engine.__name__} because of cooldown')

		if engine in active:
			active.remove(engine)

if __name__ == '__main__':
	while True:
		try:
			for i, qa in enumerate(qas):
				print(f'{qa["question"]} {qa["answer"]}')
				query = qa['question'].lower()[:-1]
				answer = qa['answer'].lower()
				try:
					for engine in active:
						print(f'Searching on {engine.__name__}...')
						with open(f'results/{engine.__name__}.json', 'a+') as output:
							try:
								results = engine(query)
								if len(results) == 0 and engine in active:
									print(f'Disabled: {engine.__name__} failed to find results.')
									active.remove(engine)
								output.write(json.dumps({
									'query': query,
									'results': results,
									'matches': score(results, answer)}) + '\n')
							except Exception as e:
								print(e)
				except Exception as e:
					print(e)

				if i % 25 == 0:
					health()
		except Exception as e:
			print(e)
			random.shuffle(qas)
			try:
				driver.close()
			except Exception:
				pass
			driver = webdriver.Firefox(options=options)
			driver.set_page_load_timeout(10)
			driver.implicitly_wait(10)
