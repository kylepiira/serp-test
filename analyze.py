import matplotlib.pyplot as plt
import numpy as np
import json
import os

engines = os.listdir('results')
data = {}
for engine in engines:
	with open(f'results/{engine}') as results:
		total = 0
		count = 0
		for result in results:
			total += json.loads(result)['matches']
			count += 1
		data[engine] = total / count
		counts[engine] = total

labels = []
values = []
for label, value in data.items():
	labels.append(label.replace('.json', '').title())
	values.append(value)

values, labels = zip(*sorted(zip(values, labels), reverse=True))

print(labels)
print(values)
print(totals)

plt.bar(np.arange(len(labels)), values, align='center', alpha=0.5)
plt.xticks(np.arange(len(labels)), labels)
plt.ylabel('Score')
plt.title('Search Engine Performance')

plt.show()